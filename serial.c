#include "serial.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stddef.h>
#include <assert.h>

HANDLE hCom = NULL;

void openSerial(char *portname)
{
    char portname_w32[255];
    int baud = 9600; /* Debit de transmission */

    DCB parametres;

    sprintf(portname_w32, "\\\\.\\%s", portname);

    if (hCom != NULL)
    {
        printf("Already have an open port, closing first\n");
        closeSerial();
    }

    hCom = CreateFile
    (
        portname_w32,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        NULL,
        NULL
    );

    if (hCom == INVALID_HANDLE_VALUE)
    {
        hCom = NULL;
        printf("Could not open serial port\n");
    }

    parametres.DCBlength = sizeof(DCB);
    GetCommState(hCom, &parametres);

    parametres.fBinary = TRUE;
    parametres.fAbortOnError = FALSE;
    parametres.fNull = FALSE;
    parametres.fErrorChar = FALSE;
    parametres.fInX = FALSE;
    parametres.fOutX = FALSE;
    parametres.fOutxCtsFlow = TRUE;
    parametres.fRtsControl = RTS_CONTROL_HANDSHAKE;

    parametres.BaudRate = baud;
    parametres.ByteSize = 7;
    parametres.Parity = 2; //0-4=no, odd, even, mark, space
    parametres.StopBits = 2; //0,1, 2=1,1.5, 2

    SetCommState(hCom, &parametres);

    SetupComm(hCom, 1024, 1024);

    printf("Communication avec le bras etablie\n");
}

void closeSerial()
{
    CloseHandle(hCom);
    hCom = NULL;
    printf("Communication avec le bras terminee\n");
}

int writeSerial(char *str)
{
    int fRes = 0;
    WriteFile(hCom, str, strlen(str), &fRes, NULL);
    return fRes;
}

int readSerial(char * buffer)
{
    int dwRead = 0;
    char * buffer_suiv = buffer;

    ReadFile(hCom, buffer_suiv, 1, &dwRead, NULL);
    while (*buffer_suiv != '\n')
    {
        ++buffer_suiv;
        ReadFile(hCom, buffer_suiv, 1, &dwRead, NULL);
    }

    buffer_suiv[0] = '\0';
    return dwRead;
}
