SRC=main.cpp
SRCC=rv-m1.c serial.c#$(wildcard *.cpp)
EXE=dessin

CXXFLAGS+=-Wall -Wextra -MMD -O2 -std=c++11 -I/usr/local/include/opencv4
LDFLAGS=-L/usr/local/lib -lopencv_xphoto -lopencv_xobjdetect -lopencv_ximgproc -lopencv_xfeatures2d -lopencv_videostab -lopencv_videoio -lopencv_video -lopencv_tracking -lopencv_surface_matching -lopencv_superres -lopencv_structured_light -lopencv_stitching -lopencv_stereo -lopencv_shape -lopencv_saliency -lopencv_rgbd -lopencv_reg -lopencv_plot -lopencv_photo -lopencv_phase_unwrapping -lopencv_optflow -lopencv_objdetect -lopencv_ml -lopencv_line_descriptor -lopencv_imgproc -lopencv_imgcodecs -lopencv_img_hash -lopencv_highgui -lopencv_hfs -lopencv_gapi -lopencv_fuzzy -lopencv_flann -lopencv_features2d -lopencv_face -lopencv_dpm -lopencv_dnn -lopencv_dnn_objdetect -lopencv_datasets -lopencv_core -std=c++11

OBJ=$(addprefix build/,$(SRC:.cpp=.o))
#OBJC=$(addprefix build/,$(SRCC:.c=.o))

all: $(OBJ) #$(OBJC)
	@echo "Compilation..."
	@$(CXX) -o $(EXE) $^ $(LDFLAGS)

build/%.o: %.cpp
	@mkdir -p build
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# build/%.o: %.c
# 	@mkdir -p build
# 	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -rf build core *.gch ._*
